
import integrator.Integrator
import factory.IntegratorFactory

import methods.rectangle.Rectangle
import methods.trapezoidal.Trapezoidal
import methods.simpson.Simpson


class Quadrature extends Integrator {

    def f

    String rule
    def integrator
    def algo

    Quadrature(rule, f) {
        this.f = f

        if (rule == 'Rectangle') {
            algo = new Rectangle(this.f)
        } else if (rule == 'Trapezoidal') {
            algo = new Trapezoidal(this.f)
        } else if (rule == 'Simpson') {
            algo = new Simpson(this.f)
        }

        this.integrator = new IntegratorFactory(algo)
    }

    def integrate(def lower, def upper, def N) {
        return this.integrator.integrate()([lower, upper, N])
    }
}

// TODO: create an user interface
def s = new Quadrature(this.args[2], {x -> x**2})
println s.integrate(Double.parseDouble(this.args[0]), Double.parseDouble(this.args[1]), Double.parseDouble(this.args[3]))