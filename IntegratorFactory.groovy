
package factory

class IntegratorFactory {
    def static factory

    IntegratorFactory(factory) {
        this.factory = factory
    }

    def static integrate()  { 
       return {it -> factory.integrate(*it)}
    }
}