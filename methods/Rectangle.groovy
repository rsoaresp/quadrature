package methods.rectangle

import integrator.Integrator

class Rectangle extends Integrator {
    
    Rectangle(f) {
        this.f = f
    }

    def integrate(def lower, def upper, def N) {
        def value = 0
        
        for (i in 0..N) {
            def k = lower + i*(upper - lower)/N
            value += f(k)
        }
        return value*(upper - lower)/N
    }
}