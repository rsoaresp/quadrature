package methods.simpson

import integrator.Integrator

class Simpson extends Integrator {
    
    Simpson(f) {
        this.f = f
    }

    def integrate(def lower, def upper, def N) {

        def value = 0
        def p = {i -> lower + i*(upper - lower)/N}

        for (i in 1..(N/2)) {
            value += f(p(2*i - 2)) + 4*f(p(2*i - 1)) + f(p(2*i))
        }
        return value*(upper - lower)/(3*N)
    }
}