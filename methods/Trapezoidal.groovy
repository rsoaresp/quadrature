package methods.trapezoidal

import integrator.Integrator

class Trapezoidal extends Integrator {

    Trapezoidal(f) {
        this.f = f
    }

    def integrate(def lower, def upper, def N) {
        def value = f(lower) + f(upper)
        
        for (i in 1..(N-1)) {
            def k = lower + i*(upper - lower)/N
            value += 2*f(k)
        }
        return 0.5*value*((upper - lower)/N)
    }
}