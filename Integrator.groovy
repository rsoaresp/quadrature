package integrator

abstract class Integrator {
    def f
    abstract def integrate(def lower, def upper, def N)
}